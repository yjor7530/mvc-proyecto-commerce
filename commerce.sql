-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-10-2021 a las 11:10:37
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `commerce`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

CREATE TABLE `acciones` (
  `Administrador_idAdministrador` int(11) NOT NULL COMMENT 'Reconocimiento sobre el administrador que realizo la accion',
  `tipAcciones` varchar(20) NOT NULL COMMENT 'que tipo de accion se realizo ',
  `fecAcciones` date NOT NULL COMMENT 'la fecha o tiempo que se realizo la accion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL COMMENT 'enumeracion sobre la cantidad de administradores',
  `nomAdministrador` int(11) NOT NULL COMMENT 'nombre del admnistrador',
  `apeAdministrador` int(11) NOT NULL COMMENT 'apellido del administrador',
  `dniAdministrador` int(11) NOT NULL COMMENT 'documento de identificacion del administrador',
  `corAdmnistrador` int(11) NOT NULL COMMENT 'correo electronico sobre el administrador',
  `useAdministrador` int(11) NOT NULL COMMENT 'usario del administrador, como pseudonimo',
  `conAdministrador` int(11) NOT NULL COMMENT 'la seguridad de su cuenta del administrador'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `idCarrito` int(11) NOT NULL COMMENT 'Para la enumeracion sobre el carrito',
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'identificacion sobre el propietario o el cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idCategoria` int(11) NOT NULL COMMENT 'enumeracion sobre las categorias que se posee',
  `nomCategoria` int(11) NOT NULL COMMENT 'identificar a que categoria nos referimos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'reconocimiento sobre el cliente',
  `fecCompras` date NOT NULL COMMENT 'la fecha en que se realizo la compra',
  `desCompras` varchar(50) NOT NULL COMMENT 'detalles sobre la compra realizada'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `idDirecciones` int(11) NOT NULL COMMENT 'Enumeracion de las direcciones de cliente',
  `nomDirecciones` varchar(40) NOT NULL COMMENT 'El nombre de las direcciones que tiene el cliente',
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'reconocimiento sobre el cliente',
  `Cliente_Carrito_idCarrito` int(11) NOT NULL COMMENT 'reconocimiento del carrito del cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPagos` int(11) NOT NULL COMMENT 'Para la enumeracion de pagos ',
  `tipPagos` varchar(40) NOT NULL COMMENT 'la forma de pago que realiza',
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'reconocimiento de cliente',
  `Cliente_Carrito_idCarrito` int(11) NOT NULL COMMENT 'reconocimiento del carrito de cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE `publicaciones` (
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'identificacion sobre el cliente',
  `idPublicaciones` int(11) NOT NULL COMMENT 'enumeracion sobre las publicaciones',
  `fecPublicaciones` date NOT NULL COMMENT 'fecha en la que se subio la publicacion',
  `fecvPublicaciones` date NOT NULL COMMENT 'fecha en donde la publicacion terminara su ciclo',
  `desPublicaciones` varchar(50) NOT NULL COMMENT 'una descripcion sobre la publicacion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'Es para reconocer al cliente',
  `Cliente_Carito_idCarrito` int(11) NOT NULL COMMENT 'Es para reconocer e informar sobre el pedido',
  `numTelefono` int(11) NOT NULL COMMENT 'el numero del cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `Apellido` varchar(25) NOT NULL,
  `Dni` int(8) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(25) NOT NULL,
  `Cargo` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Nombre`, `Apellido`, `Dni`, `Correo`, `Username`, `Password`, `Cargo`) VALUES
(1, 'Javier', 'Zamata', 75208484, 'jzamatac', 'jj753', 'qwe', 'estudiante'),
(2, 'Javier', 'Zamata', 75208414, 'jzamatac2', 'jjj', '12345678', 'Estudiante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `Cliente_idCliente` int(11) NOT NULL COMMENT 'identificacion sobre el cliente',
  `fecVentas` int(11) NOT NULL COMMENT 'fecha en la que se realizo la venta',
  `desVentas` int(11) NOT NULL COMMENT 'un descripcion pequeña sobre la venta realizada'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`,`Password`),
  ADD UNIQUE KEY `Dni` (`Dni`),
  ADD UNIQUE KEY `Correo` (`Correo`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
