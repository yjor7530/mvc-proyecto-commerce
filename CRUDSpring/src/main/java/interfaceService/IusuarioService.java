package interfaceService;

import java.util.List;
import java.util.Optional;
import modelo.Usuario;

//CRUD
public interface IusuarioService {
	public List<Usuario> listar();
	public Optional<Usuario> listarId(int id);
	public int save(Usuario aux);
	public void delete(int id);
}
