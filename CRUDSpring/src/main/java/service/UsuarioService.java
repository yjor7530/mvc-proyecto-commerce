package service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import interfaceService.IusuarioService;
import interfaces.IUsuario;
import modelo.Usuario;

@Service
public class UsuarioService implements IusuarioService{

	@Autowired
	private IUsuario data;
	
	@Override
	public List<Usuario> listar() {
		return (List<Usuario>)data.findAll();
	}

	@Override
	public Optional<Usuario> listarId(int id) {
		return data.findById(id);
	}

	@Override
	public int save(Usuario aux) {
		int res=0;
		Usuario usr=data.save(aux);
		if(!usr.equals(null)) {
			res=1;
		}
		return res;
	}

	@Override
	public void delete(int id) {
		data.deleteById(id);
	}



}
