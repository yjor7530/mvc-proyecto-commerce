package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public int Id;
    public String Nombre; 
    public String Apellido;
    public int DNI;
    public String Correo;
    public String Username;
    public String Password;
    public String Cargo;
    
    public Usuario() {
    	
    }

	public Usuario(int id, String nombre, String apellido, int dni, String correo, String username, String password,
			String cargo) {
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.Apellido = apellido;
		this.DNI = dni;
		this.Correo = correo;
		this.Username = username;
		this.Password = password;
		this.Cargo = cargo;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		this.Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		this.Nombre = nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		this.Apellido = apellido;
	}

	public int getDNI() {
		return DNI;
	}

	public void setDNI(int dNI) {
		this.DNI = dNI;
	}

	public String getCorreo() {
		return Correo;
	}

	public void setCorreo(String correo) {
		this.Correo = correo;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		this.Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		this.Password = password;
	}

	public String getCargo() {
		return Cargo;
	}

	public void setCargo(String cargo) {
		this.Cargo = cargo;
	}
    
    
}
